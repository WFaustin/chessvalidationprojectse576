Widchard Faustin
Professor Vokolos
SE 576

August 20th, 2019

Software Engineering 576 Project Assignment
README

Purpose:
	
	The purpose of this README is to describe and give an explanation of the program that I
developed and tested, how to build both the project and test scenarios and run either of them, and 
any shortcomings with my software. 

	This program is a Chess Move Generator. It takes an input of white pieces, black pieces, and the piece that should be specified to move, through a group of strings. It then creates pieces on a
chess board based on the strings, and tries to find the piece to move. If it is not found, the program outputs that message and ends, otherwise, it outputs all legal moves, and then ends. 


Instructions: 
	Java Version: 1.8.0_161, SDK default

	This program was built with Intellij IDEA Ultimate Edition and Intellij IDEA Community Edition. 
As such, there is no makefile or ant script included with this project. I did speak to the TA about if either of these documents needed to be included, and I received a no (although he did say it would be helpful. Unfortunately, due to time constraints, I couldn\'92t manage to fit one in). As such, the
instructions to run and load the project are the following:

	1). Download and open Intellij IDEA (Ultimate or Community Edition)

	2). Download the zip file containing the project. 

	3). Unzip the project, and then open the project in IntelliJ.

	4). Navigate to the main.java class, as well as the maintest.java class in the test folder.

	5). On line 5 of Main.java, to the border of the left, there should be a run button. Click that to
run the program with user input. On line 5 of MainTest.java,  to the border of the left, there should be a run button. Click that to run the program with simulated input.
	
Basic Structure of Inputs:

	Each Piece on the Virtual Chessboard has 3 Inputs: A character denoting the type of
piece; a character denoting the row in which the piece is located, and a single digit int to denote the
column of the piece. This goes in the form of a three character string. 
		
	First Input:
		R/r - Rook; B/b - Bishop; K/k - Knight (Note: Different input for knight than what was originally detailed in the project assignment CHESS word doc)
; Q/q - Queen; P/p - Pawn; C/c - King (Note: Different input for king than what was originally detailed in the project assignment CHESS word doc)
		
	Second Input:
			
		A/a - H/h signifies the rows of the chess board, from 1 - 8. 
		
	Third Input:

		1 - 8 signifies the columns of the chessboard from 1 - 8. 
	
		After a piece is created, a space is inputted to signify the next piece. Note, the program
will error out if the input is incorrect. I didn't have time to get a full error handler working, so that is
what currently happens. 

		
	Example Inputs:
			
	PF3 Cb5 ka6 - 3 pieces will be made, a Pawn at F3, a King at B5, and a Knight
at A6. 
			bA3 - 1 piece will be made, a Bishop at A3. 


Shortcomings:
	
	Although I personally feel like I accomplished a lot with this project, there are some specific
things that I omitted in this project because of lack of time or lack of full understanding. For example,
one thing that I didn't do in my test cases is make specific unit tests. Part of the reason that I did this
was because of the fact that each of the separate pieces were debugged during my development, in
my eyes, sufficiently, and trying to remake those unit tests to overall test the project was going to take
too long, and I wouldn't be able to finish. The other reason was because I mapped out how the
squares that a piece could move to should be properly outputted, so I felt that it would be easier to 
catch a mistake that way. 

	Another shortcoming of my project is the lack of a makefile/ant script, which I mentioned before
was due to time constraints. 
