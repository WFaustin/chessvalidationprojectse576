package com.company.Tests;

import com.company.Input;
import com.company.Main;
import com.company.ValidMoveGenerator;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.Test
    //This test is 2 pawns, opposite sides, blocking one another.
    void twoPawns() {
        assert(methodForRunningTestsUsingInputandOutput("PB1\nPb2\npb1\n").equals("\n" +
                "This Is a Chess Move Validation Simulation. Please do the following:\n" +
                "Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter the piece that should be checked for moves.\n" +
                "Printing out valid moves.\n" +
                "The white pawn at square 1, 0 can move in the following places.\n" +
                "There could not be ay valid moves found on the board."));
    }

    @org.junit.jupiter.api.Test
    //This test is 3 pawns on white side, 2 on black, blocking one another, in order to demonstrate en passant.
    void bunchofPawns(){
        assert(methodForRunningTestsUsingInputandOutput("PA1 pB1 PC1\npA2 PC2\npB1\n").equals("\n" +
                "This Is a Chess Move Validation Simulation. Please do the following:\n" +
                "Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter the piece that should be checked for moves.\n" +
                "Printing out valid moves.\n" +
                "The white pawn at square 1, 0 can move in the following places.\n" +
                "B2\n" +
                "A2\n" +
                "C2"));
    }
    @org.junit.jupiter.api.Test
        //This test just demonstrates pieces and how they can move in the middle of the board.
    void soloPiecesInTheMiddleofTheBoard(){
        assert(methodForRunningTestsUsingInputandOutput("qd3\nPf7\nQD3").equals("\n" +
                "This Is a Chess Move Validation Simulation. Please do the following:\n" +
                "Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter the piece that should be checked for moves.\n" +
                "Printing out valid moves.\n" +
                "The white queen at square 3, 2 can move in the following places.\n" +
                "E4\n" +
                "C4\n" +
                "E2\n" +
                "C2\n" +
                "E3\n" +
                "C3\n" +
                "D2\n" +
                "D4\n" +
                "F5\n" +
                "B5\n" +
                "F1\n" +
                "B1\n" +
                "F3\n" +
                "B3\n" +
                "D1\n" +
                "D5\n" +
                "G6\n" +
                "A6\n" +
                "G3\n" +
                "A3\n" +
                "D6\n" +
                "H7\n" +
                "H3\n" +
                "D7\n" +
                "D8"));
        assert(methodForRunningTestsUsingInputandOutput("Bd3\npF7\nBd3").equals("\n" +
                "This Is a Chess Move Validation Simulation. Please do the following:\n" +
                "Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter the piece that should be checked for moves.\n" +
                "Printing out valid moves.\n" +
                "The white bishop at square 3, 2 can move in the following places.\n" +
                "E4\n" +
                "C4\n" +
                "E2\n" +
                "C2\n" +
                "F5\n" +
                "B5\n" +
                "F1\n" +
                "B1\n" +
                "G6\n" +
                "A6\n" +
                "H7"));
        assert(methodForRunningTestsUsingInputandOutput("Rd3\npF7\nRd3").equals("\n" +
                "This Is a Chess Move Validation Simulation. Please do the following:\n" +
                "Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter the piece that should be checked for moves.\n" +
                "Printing out valid moves.\n" +
                "The white rook at square 3, 2 can move in the following places.\n" +
                "E3\n" +
                "C3\n" +
                "D2\n" +
                "D4\n" +
                "F3\n" +
                "B3\n" +
                "D1\n" +
                "D5\n" +
                "G3\n" +
                "A3\n" +
                "D6\n" +
                "H3\n" +
                "D7\n" +
                "D8"));
        assert(methodForRunningTestsUsingInputandOutput("Kd3\npF7\nKd3").equals("\n" +
                "This Is a Chess Move Validation Simulation. Please do the following:\n" +
                "Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter the piece that should be checked for moves.\n" +
                "Printing out valid moves.\n" +
                "The white knight at square 3, 2 can move in the following places.\n" +
                "F4\n" +
                "B4\n" +
                "F2\n" +
                "B2\n" +
                "E5\n" +
                "C5\n" +
                "E1\n" +
                "C1"));
        assert(methodForRunningTestsUsingInputandOutput("Cd3\npF7\nCd3").equals("\n" +
                "This Is a Chess Move Validation Simulation. Please do the following:\n" +
                "Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter the piece that should be checked for moves.\n" +
                "Printing out valid moves.\n" +
                "The white king at square 3, 2 can move in the following places.\n" +
                "E4\n" +
                "E2\n" +
                "C2\n" +
                "C4\n" +
                "D4\n" +
                "D2\n" +
                "E3\n" +
                "C3"));
        assert(methodForRunningTestsUsingInputandOutput("Pd3\npF7\nPd3").equals("\n" +
                "This Is a Chess Move Validation Simulation. Please do the following:\n" +
                "Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter the piece that should be checked for moves.\n" +
                "Printing out valid moves.\n" +
                "The white pawn at square 3, 2 can move in the following places.\n" +
                "D4"));
    }

    @org.junit.jupiter.api.Test
        //This test just demonstrates pieces and how they can move in the middle of the board.
    void soloPiecesOnTheCornerfTheBoard() {
        assert(methodForRunningTestsUsingInputandOutput("QA1\npF7\nQA1").equals("\n" +
                "This Is a Chess Move Validation Simulation. Please do the following:\n" +
                "Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter the piece that should be checked for moves.\n" +
                "Printing out valid moves.\n" +
                "The white queen at square 0, 0 can move in the following places.\n" +
                "B2\n" +
                "B1\n" +
                "A2\n" +
                "C3\n" +
                "C1\n" +
                "A3\n" +
                "D4\n" +
                "D1\n" +
                "A4\n" +
                "E5\n" +
                "E1\n" +
                "A5\n" +
                "F6\n" +
                "F1\n" +
                "A6\n" +
                "G7\n" +
                "G1\n" +
                "A7\n" +
                "H8\n" +
                "H1\n" +
                "A8"));
        assert(methodForRunningTestsUsingInputandOutput("BA1\npF7\nBA1").equals("\n" +
                "This Is a Chess Move Validation Simulation. Please do the following:\n" +
                "Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter the piece that should be checked for moves.\n" +
                "Printing out valid moves.\n" +
                "The white bishop at square 0, 0 can move in the following places.\n" +
                "B2\n" +
                "C3\n" +
                "D4\n" +
                "E5\n" +
                "F6\n" +
                "G7\n" +
                "H8"));
        assert(methodForRunningTestsUsingInputandOutput("RA1\npF7\nRA1").equals("\n" +
                "This Is a Chess Move Validation Simulation. Please do the following:\n" +
                "Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter the piece that should be checked for moves.\n" +
                "Printing out valid moves.\n" +
                "The white rook at square 0, 0 can move in the following places.\n" +
                "B1\n" +
                "A2\n" +
                "C1\n" +
                "A3\n" +
                "D1\n" +
                "A4\n" +
                "E1\n" +
                "A5\n" +
                "F1\n" +
                "A6\n" +
                "G1\n" +
                "A7\n" +
                "H1\n" +
                "A8"));
        assert(methodForRunningTestsUsingInputandOutput("KA1\npF7\nKA1").equals("\n" +
                "This Is a Chess Move Validation Simulation. Please do the following:\n" +
                "Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter the piece that should be checked for moves.\n" +
                "Printing out valid moves.\n" +
                "The white knight at square 0, 0 can move in the following places.\n" +
                "C2\n" +
                "B3"));
        assert(methodForRunningTestsUsingInputandOutput("CA1\npF7\nCA1").equals("\n" +
                "This Is a Chess Move Validation Simulation. Please do the following:\n" +
                "Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter the piece that should be checked for moves.\n" +
                "Printing out valid moves.\n" +
                "The white king at square 0, 0 can move in the following places.\n" +
                "B2\n" +
                "A2\n" +
                "B1"));
        assert(methodForRunningTestsUsingInputandOutput("PA1\npF7\nPA1").equals("\n" +
                "This Is a Chess Move Validation Simulation. Please do the following:\n" +
                "Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter the piece that should be checked for moves.\n" +
                "Printing out valid moves.\n" +
                "The white pawn at square 0, 0 can move in the following places.\n" +
                "A2"));
        assert(methodForRunningTestsUsingInputandOutput("Pf1\npA1\nPA1").equals("\n" +
                "This Is a Chess Move Validation Simulation. Please do the following:\n" +
                "Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter the piece that should be checked for moves.\n" +
                "Printing out valid moves.\n" +
                "The black pawn at square 0, 0 can move in the following places.\n" +
                "There could not be ay valid moves found on the board."));
    }

    @org.junit.jupiter.api.Test
        //This test deals with kings in different places
    void kingsInDifferentPositions(){
        assert(methodForRunningTestsUsingInputandOutput("KA8\ncb7\nCB7\n").equals("\n" +
                "This Is a Chess Move Validation Simulation. Please do the following:\n" +
                "Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter the piece that should be checked for moves.\n" +
                "Printing out valid moves.\n" +
                "The black king at square 1, 6 can move in the following places.\n" +
                "C8\n" +
                "C6\n" +
                "A6\n" +
                "A8\n" +
                "B8\n" +
                "A7"));
        assert(methodForRunningTestsUsingInputandOutput("BA8\nch8\nCH8\n").equals("\n" +
                "This Is a Chess Move Validation Simulation. Please do the following:\n" +
                "Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter the piece that should be checked for moves.\n" +
                "Printing out valid moves.\n" +
                "The black king at square 7, 7 can move in the following places.\n" +
                "G7\n" +
                "H7\n" +
                "G8"));
        assert(methodForRunningTestsUsingInputandOutput("re2\nce5\nCE5\n").equals("\n" +
                "This Is a Chess Move Validation Simulation. Please do the following:\n" +
                "Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).\n" +
                "Enter the piece that should be checked for moves.\n" +
                "Printing out valid moves.\n" +
                "The black king at square 4, 4 can move in the following places.\n" +
                "F6\n" +
                "F4\n" +
                "D4\n" +
                "D6\n" +
                "E6\n" +
                "F5\n" +
                "D5"));
    }

    String methodForRunningTestsUsingInputandOutput(String message){
        Input input = Input.getInstance();
        if(!input.getOutput().getOutput().equals("")){
            input.getOutput().flushAllOutput();
        }
        InputStream in = new ByteArrayInputStream(message.getBytes());
        System.setIn(in);
        Scanner scanner = new Scanner(System.in);
        input.setScanner(scanner);
        new ValidMoveGenerator();
        return input.getOutput().getOutput();
    }


}