package com.company;

import java.awt.*;

public class Bishop extends Piece {

    public Bishop(String name, String color, Point position){
        super(name, color);
        piece_position = position;
        generateAllPossibleMoves();
    }

    @Override
    public void generateAllPossibleMoves() {
        waystoMove = new Point[28];
        int pointcounter = 1;
        for (int i = 0; i < waystoMove.length; i = i+4){
            waystoMove[i] = new Point(pointcounter, pointcounter);
            waystoMove[i+1] = new Point(-pointcounter, pointcounter);
            waystoMove[i+2] = new Point(pointcounter, -pointcounter);
            waystoMove[i+3] = new Point(-pointcounter, -pointcounter);
            pointcounter++;
        }
    }
}
