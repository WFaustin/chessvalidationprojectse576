package com.company;

public class Converter {

    private String string;

    public Converter(){
        string = "";
    }

    public String convertStringforInput(){
        char firstchar = string.charAt(0);
        char secondchar = string.charAt(1);
        switch (secondchar){
            case ('A'):
                secondchar = '0';
                break;
            case ('a'):
                secondchar = '0';
                break;
            case ('B'):
                secondchar = '1';
                break;
            case ('b'):
                secondchar = '1';
                break;
            case ('C'):
                secondchar = '2';
                break;
            case ('c'):
                secondchar = '2';
                break;
            case ('D'):
                secondchar = '3';
                break;
            case ('d'):
                secondchar = '3';
                break;
            case ('E'):
                secondchar = '4';
                break;
            case ('e'):
                secondchar = '4';
                break;
            case ('F'):
                secondchar = '5';
                break;
            case ('f'):
                secondchar = '5';
                break;
            case ('G'):
                secondchar = '6';
                break;
            case ('g'):
                secondchar = '6';
                break;
            case ('H'):
                secondchar = '7';
                break;
            case ('h'):
                secondchar = '7';
                break;
            default:
                break;
        }
        char thirdchar = string.charAt(2);
        switch (thirdchar){
            case ('1'):
                thirdchar = '0';
                break;
            case ('2'):
                thirdchar = '1';
                break;
            case ('3'):
                thirdchar = '2';
                break;
            case ('4'):
                thirdchar = '3';
                break;
            case ('5'):
                thirdchar = '4';
                break;
            case ('6'):
                thirdchar = '5';
                break;
            case ('7'):
                thirdchar = '6';
                break;
            case ('8'):
                thirdchar = '7';
                break;
            default:
                break;
        }
        firstchar = Character.toUpperCase(firstchar);
        string = Character.toString(firstchar) + Character.toString(secondchar) + Character.toString(thirdchar);
        return string;
    }

    public String convertStringforOutput(){
        char secondchar = string.charAt(0);
        switch (secondchar){
            case ('0'):
                secondchar = 'A';
                break;
            case ('1'):
                secondchar = 'B';
                break;
            case ('2'):
                secondchar = 'C';
                break;
            case ('3'):
                secondchar = 'D';
                break;
            case ('4'):
                secondchar = 'E';
                break;
            case ('5'):
                secondchar = 'F';
                break;
            case ('6'):
                secondchar = 'G';
                break;
            case ('7'):
                secondchar = 'H';
                break;
            default:
                break;
        }
        char thirdchar = string.charAt(1);
        switch (thirdchar){
            case ('0'):
                thirdchar = '1';
                break;
            case ('1'):
                thirdchar = '2';
                break;
            case ('2'):
                thirdchar = '3';
                break;
            case ('3'):
                thirdchar = '4';
                break;
            case ('4'):
                thirdchar = '5';
                break;
            case ('5'):
                thirdchar = '6';
                break;
            case ('6'):
                thirdchar = '7';
                break;
            case ('7'):
                thirdchar = '8';
                break;
            default:
                break;
        }

        string = Character.toString(secondchar) + Character.toString(thirdchar);
        return string;
    }

    public String setStringForOutput(String string){
        this.string = string;
        return convertStringforOutput();
    }

    public String setStringForInput(String string){
        this.string = string;
        return convertStringforInput();
    }
}
