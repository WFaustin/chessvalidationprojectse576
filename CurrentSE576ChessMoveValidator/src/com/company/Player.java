package com.company;

import java.awt.*;
import java.util.Vector;

public class Player {

    private String color;
    private Vector<Piece> pieces;

    public Player(String color){
        this.color = color;
        pieces = new Vector<Piece>();
    }

    public Piece getPiece(int num){
        return pieces.get(num);
    }


    public Piece createPiece(String piece_identifier, int row_input, int column_input){
        switch (piece_identifier){
            case "P":
                pieces.add(new Pawn("pawn", color, new Point(row_input, column_input)));
                break;
            case "p":
                pieces.add(new Pawn("pawn", color, new Point(row_input, column_input)));
                break;
            case "R":
                pieces.add(new Rook("rook", color, new Point(row_input, column_input)));
                break;
            case "r":
                pieces.add(new Rook("rook", color, new Point(row_input, column_input)));
                break;
            case "K":
                pieces.add(new Knight("knight", color, new Point(row_input, column_input)));
                break;
            case "k":
                pieces.add(new Knight("knight", color, new Point(row_input, column_input)));
                break;
            case "B":
                pieces.add(new Bishop("bishop", color, new Point(row_input, column_input)));
                break;
            case "b":
                pieces.add(new Bishop("bishop", color, new Point(row_input, column_input)));
                break;
            case "Q":
                pieces.add(new Queen("queen", color, new Point(row_input, column_input)));
                break;
            case "q":
                pieces.add(new Queen("queen", color, new Point(row_input, column_input)));
                break;
            case "C":
                pieces.add( new King("king", color, new Point(row_input, column_input)));
                break;
            case "c":
                pieces.add( new King("king", color, new Point(row_input, column_input)));
                break;
            default:
                return null;
        }
        return pieces.get(pieces.size() - 1);
    }

}
