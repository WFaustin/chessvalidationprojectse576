package com.company;

import java.awt.*;

public class Rook extends Piece {

    public Rook(String name, String color, Point position){
        super(name, color);
        piece_position = position;
        generateAllPossibleMoves();
    }


    public void generateAllPossibleMoves() {
        waystoMove = new Point[28];
        int pointcounter = 1;
        for (int i = 0; i < waystoMove.length; i = i+4){
            waystoMove[i] = new Point(pointcounter, 0);
            waystoMove[i+1] = new Point(-pointcounter, 0);
            waystoMove[i+2] = new Point(0, -pointcounter);
            waystoMove[i+3] = new Point(0, pointcounter);
            pointcounter++;
        }
    }
}
