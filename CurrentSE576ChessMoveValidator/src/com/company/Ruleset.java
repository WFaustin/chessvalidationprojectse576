package com.company;

import java.awt.*;
import java.util.ArrayList;
import java.util.Vector;

public class Ruleset {

    //Adding all needed checks for ruleset
    public Ruleset(){
    }

    public Point[] getValidMoveForPiece(Piece piece, Board board) {
        //returns an array of all the valid points the piece can move to
        ArrayList<Point> notKnownValidMoves = new ArrayList<Point>();
        for (int i = 0; i < piece.getWaystoMove().length; i++){
            if (isSingleValidMove(piece, piece.getWaystoMove()[i], board)){
                notKnownValidMoves.add(piece.getWaystoMove()[i]);
            }
        }
        //check to make sure if the king is moved, it hasn't been moved into check
        if(piece.getPiece_name().equals("king")){
            ArrayList<Point> notKnownValidMovesForOpponent = new ArrayList<Point>();
            Vector<Piece> opposingPieces = board.getPiecesofOneSide(board.opposingColor(piece.getPiece_color()));
            for (int i = 0; i < opposingPieces.size(); i++){
                for (int j = 0; j < opposingPieces.get(i).getWaystoMove().length; j++) {
                    if (isSingleValidMove(opposingPieces.get(i), opposingPieces.get(i).getWaystoMove()[j], board)) {
                        Point opposingPiecePosition = new Point(opposingPieces.get(i).getPiece_position().x + opposingPieces.get(i).getWaystoMove()[j].x, opposingPieces.get(i).getPiece_position().y + opposingPieces.get(i).getWaystoMove()[j].y);
                        notKnownValidMovesForOpponent.add(opposingPiecePosition);
                    }
                }
            }
            ArrayList<Point> transitionVector = new ArrayList<Point>();
            for (int i = 0; i < notKnownValidMoves.size(); i++){
                transitionVector.add(notKnownValidMoves.get(i));
                for (int j = 0; j < notKnownValidMovesForOpponent.size(); j++){
                    if ((piece.getPiece_position().x + notKnownValidMoves.get(i).x == notKnownValidMovesForOpponent.get(j).x) && (piece.getPiece_position().y + notKnownValidMoves.get(i).y == notKnownValidMovesForOpponent.get(j).y)){
                        transitionVector.remove(notKnownValidMoves.get(i));
                        notKnownValidMovesForOpponent.remove(notKnownValidMovesForOpponent.get(j));
                        break;
                    }
                }
            }
            notKnownValidMoves = transitionVector;
        }

        Point [] knownValidMoves = new Point [notKnownValidMoves.size()];
        for (int i = 0; i < knownValidMoves.length; i++){
            knownValidMoves[i] = notKnownValidMoves.get(i);
        }
        return knownValidMoves;
    }

    public boolean isSingleValidMove(Piece piece, Point move, Board board){
        if (board.getBoard().length <= (piece.getPiece_position().x + move.x) || board.getBoard().length <= (piece.getPiece_position().y + move.y) || (piece.getPiece_position().x + move.x) < 0 ||  (piece.getPiece_position().y + move.y) < 0){
            return false;
        }
        else if(piece.getPiece_name().equals("pawn") && isSingleValidMovePawn(piece, move, board)){
            return true;
        }
        else if(piece.getPiece_name().equals("pawn") && !isSingleValidMovePawn(piece, move, board)){
            return false;
        }
        else if (board.isSquareEmpty(piece.getPiece_position().x + move.x, piece.getPiece_position().y + move.y) == true && pieceInBetween(piece, move, board) == false){
            return true;
        }
        else if(board.isSquareEmpty(piece.getPiece_position().x + move.x, piece.getPiece_position().y + move.y) == true && pieceInBetween(piece, move, board) == true){
            return false;
        }
        else if (board.isSquareEmpty(piece.getPiece_position().x + move.x, piece.getPiece_position().y + move.y) == false && board.getPiece(piece.getPiece_position().x + move.x, piece.getPiece_position().y + move.y).getPiece_color().equals(piece.getPiece_color())){
            return false;
        }
        else if (board.isSquareEmpty(piece.getPiece_position().x + move.x, piece.getPiece_position().y + move.y) == false && !board.getPiece(piece.getPiece_position().x + move.x, piece.getPiece_position().y + move.y).getPiece_color().equals(piece.getPiece_color()) && pieceInBetween(piece, move, board) == true){
            return false;
        }
        else if (board.isSquareEmpty(piece.getPiece_position().x + move.x, piece.getPiece_position().y + move.y) == false && !board.getPiece(piece.getPiece_position().x + move.x, piece.getPiece_position().y + move.y).getPiece_color().equals(piece.getPiece_color()) && !pieceInBetween(piece, move, board) == true && !piece.getPiece_name().equals("pawn")){
            return true;
        }
        else if (board.isSquareEmpty(piece.getPiece_position().x + move.x, piece.getPiece_position().y + move.y) == false && !board.getPiece(piece.getPiece_position().x + move.x, piece.getPiece_position().y + move.y).getPiece_color().equals(piece.getPiece_color()) && !pieceInBetween(piece, move, board) == true && piece.getPiece_name().equals("pawn")){
            return false;
        }
        else{
            return false;
        }
    }

    public boolean isSingleValidMovePawn(Piece piece, Point move, Board board){
        boolean isWhite;
        if (piece.getPiece_color().equals("white")){
            isWhite = true;
        }
        else{
            isWhite = false;
        }
        if (move.x == 0 && !board.isSquareEmpty(piece.getPiece_position().x + move.x, piece.getPiece_position().y + move.y)){
            return false;
        }
        else if (move.x == 0 && board.isSquareEmpty(piece.getPiece_position().x + move.x, piece.getPiece_position().y + move.y)){
            return true;
        }
        else if (Math.abs(move.x) == 1 && board.isSquareEmpty(piece.getPiece_position().x + move.x, piece.getPiece_position().y + move.y)){
            return false;
        }
        else if (Math.abs(move.x) == 1 && !board.isSquareEmpty(piece.getPiece_position().x + move.x, piece.getPiece_position().y + move.y) && board.getPiece(piece.getPiece_position().x + move.x, piece.getPiece_position().y + move.y).getPiece_color().equals(piece.getPiece_color())){
            return false;
        }
        else if (Math.abs(move.x) == 1 && !board.isSquareEmpty(piece.getPiece_position().x + move.x, piece.getPiece_position().y + move.y) && !board.getPiece(piece.getPiece_position().x + move.x, piece.getPiece_position().y + move.y).getPiece_color().equals(piece.getPiece_color())){
            return true;
        }
        else{
            return false;
        }
    }

    public boolean pieceInBetween(Piece piece, Point move, Board board){
        if (piece.piece_name == "knight"){
            return false;
        }
        else if(Math.abs(move.x) == 1 || Math.abs(move.y) == 1){
            return false;
        }
        else if(Math.abs(move.x) == Math.abs(move.y)){
            //diagonal
            return diagonalMoveCheck(piece, new Point(piece.piece_position.x + move.x, piece.piece_position.y + move.y), board, true);
        }
        else{
            //horizontal and/or vertical
            return horizontalMoveCheck(piece, new Point(piece.piece_position.x + move.x, piece.piece_position.y + move.y), board, false);
        }
    }

    public boolean diagonalMoveCheck(Piece piece, Point move, Board board, boolean isDiagonal){
        if (move.x > piece.getPiece_position().x && move.y > piece.getPiece_position().y){
            int newComparison = move.x - piece.getPiece_position().x;
            for (int i = 1; i < newComparison; i++){
                if(board.isSquareEmpty(piece.getPiece_position().x + i, piece.getPiece_position().y + i) == false){
                    return true;
                }
            }
            return false;
        }
        else if (move.x > piece.getPiece_position().x && move.y < piece.getPiece_position().y){
            int newComparison = move.x - piece.getPiece_position().x;
            for (int i = 1; i < newComparison; i++){
                if(board.isSquareEmpty(piece.getPiece_position().x + i, piece.getPiece_position().y - i) == false){
                    return true;
                }
            }
            return false;
        }
        else if (move.x < piece.getPiece_position().x && move.y < piece.getPiece_position().y){
            int newComparison = move.x - piece.getPiece_position().x;
            for (int i = 1; i < newComparison; i++){
                if(board.isSquareEmpty(piece.getPiece_position().x - i, piece.getPiece_position().y - i) == false){
                    return true;
                }
            }
            return false;
        }
        else if (move.x < piece.getPiece_position().x && move.y > piece.getPiece_position().y){
            int newComparison = move.x - piece.getPiece_position().x;
            for (int i = 1; i < newComparison; i++){
                if(board.isSquareEmpty(piece.getPiece_position().x - i, piece.getPiece_position().y + i) == false){
                    return true;
                }
            }
            return false;
        }
        else{
            return false;
        }
    }

    public boolean horizontalMoveCheck(Piece piece, Point move, Board board, boolean isDiagonal){
        if (!isDiagonal && move.x == piece.getPiece_position().x && move.y > 0){
            int newComparison = move.y - piece.getPiece_position().y;
            for (int i = 1; i < newComparison; i++){
                if (!board.isSquareEmpty(piece.getPiece_position().x, i + piece.getPiece_position().y)){
                    return true;
                }
            }
        }
        else if (!isDiagonal && move.x == piece.getPiece_position().x && move.y < 0){
            int newComparison = move.y - piece.getPiece_position().y;
            for (int i = 1; i < Math.abs(newComparison); i++){
                if (!board.isSquareEmpty(piece.getPiece_position().x, -i + piece.getPiece_position().y)){
                    return true;
                }
            }
        }
        else if (!isDiagonal && move.y == piece.getPiece_position().y && move.x < 0){
            int newComparison = move.x - piece.getPiece_position().x;
            for (int i = 1; i < Math.abs(newComparison); i++){
                if (!board.isSquareEmpty(-i + piece.getPiece_position().x, piece.getPiece_position().y)){
                    return true;
                }
            }
        }
        else if (!isDiagonal && move.y == piece.getPiece_position().y && move.x > 0){
            int newComparison = move.x - piece.getPiece_position().x;
            for (int i = 1; i < newComparison; i++){
                if (!board.isSquareEmpty(i + piece.getPiece_position().x, piece.getPiece_position().y)){
                    return true;
                }
            }
        }
        else{
            return false;
        }
        return false;
    }


}
