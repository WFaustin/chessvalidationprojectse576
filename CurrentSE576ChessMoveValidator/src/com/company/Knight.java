package com.company;

import java.awt.*;

public class Knight extends Piece{

    public Knight(String name, String color, Point position){
        super(name, color);
        waystoMove = new Point[8];
        piece_position = position;
        waystoMove[0] = new Point(2, 1);
        waystoMove[1] = new Point(-2, 1);
        waystoMove[2] = new Point(2, -1);
        waystoMove[3] = new Point(-2, -1);
        waystoMove[4] = new Point(1, 2);
        waystoMove[5] = new Point(-1, 2);
        waystoMove[6] = new Point(1, -2);
        waystoMove[7] = new Point(-1, -2);
    }
}

