package com.company;

import java.awt.*;

public class Queen extends Piece {

    public Queen(String name, String color, Point position){
        super(name, color);
        piece_position = position;
        generateAllPossibleMoves();
    }

    @Override
    public void generateAllPossibleMoves() {
        waystoMove = new Point[56];
        int pointcounter = 1;
        for (int i = 0; i < waystoMove.length; i = i+8){
            waystoMove[i] = new Point(pointcounter, pointcounter);
            waystoMove[i+1] = new Point(-pointcounter, pointcounter);
            waystoMove[i+2] = new Point(pointcounter, -pointcounter);
            waystoMove[i+3] = new Point(-pointcounter, -pointcounter);
            waystoMove[i+4] = new Point(pointcounter, 0);
            waystoMove[i+5] = new Point(-pointcounter, 0);
            waystoMove[i+6] = new Point(0, -pointcounter);
            waystoMove[i+7] = new Point(0, pointcounter);
            pointcounter++;
        }
    }
}

