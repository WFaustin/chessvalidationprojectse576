package com.company;

import java.util.Scanner;

public class Input {

    private Scanner scanner = new Scanner(System.in);
    private Output output;

    private static final Input SINGLE_INSTANCE = new Input();

    private Input(){
        output = new Output();
    }

    public static Input getInstance() {
        return SINGLE_INSTANCE;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public void setScanner(Scanner s){
        scanner = s;
    }

    public Output getOutput() {
        return output;
    }
}
