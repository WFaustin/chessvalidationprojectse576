package com.company;

import java.awt.*;

public class Piece {
    //The pieces know how they need to move and where they are, and we can pass this information off to the board and ruleset instead of the other way arround
    protected Point[] waystoMove;
    protected String piece_name;
    protected String piece_color;
    protected Point piece_position;

    public Piece(String name, String color){
        piece_name = name;
        piece_color = color;
    }

    public String getPiece_name(){
        return piece_name;
    }

    public String getPiece_color() {
        return piece_color;
    }

    public Point getPiece_position() {
        return piece_position;
    }

    public Point[] getWaystoMove(){
        return waystoMove;
    }

    public void generateAllPossibleMoves(){

    }
}
