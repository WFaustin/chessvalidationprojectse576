package com.company;

public class Output {

    private String collectallOutput;

    public Output(){
        collectallOutput = "";
    }

    public void printOutput(String message){
        concatenateAllOutput(message);
        System.out.println(message);
    }

    public String getOutput(){
        return collectallOutput;
    }

    private void concatenateAllOutput(String message){
        collectallOutput += "\n" + message;
    }

    public void flushAllOutput(){
        collectallOutput = "";
    }

}
