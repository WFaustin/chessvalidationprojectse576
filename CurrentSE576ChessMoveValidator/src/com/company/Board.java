package com.company;

import java.util.Vector;

public class Board {
    private Piece [][] board;

    public Board(){
        board = new Piece[8][8];
    }

    public Piece[][] getBoard() {
        return board;
    }

    public Piece getPiece(int x, int y){
        return board[x][y];
    }

    public boolean putPieceOnBoard(Piece piece, int x, int y){
        if (!isSquareEmpty(x, y)){
            return false;
        }
        else{
            board[x][y] = piece;
            return true;
        }
    }

    public boolean isSquareEmpty(int x, int y){
        if (getPiece(x, y) == null){
            return true;
        }
        else{
            return false;
        }
    }

    public Vector<Piece> getPiecesofOneSide(String color){
        Vector<Piece> pieces = new Vector<Piece>();
        for (int i = 0; i < board.length; i++){
            for (int j = 0; j < board.length; j++){
                if (!isSquareEmpty(i, j) && getPiece(i, j).getPiece_color().equals(color)){
                    pieces.add(getPiece(i, j));
                }
            }
        }
        return pieces;
    }

    public String opposingColor(String color){
        if (color.equals("black")){
            return "white";
        }
        else if (color.equals("white")){
            return "black";
        }
        else{
            return null;
        }
    }


}
