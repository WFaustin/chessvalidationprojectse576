package com.company;

import java.awt.*;

public class Pawn extends Piece {

    public Pawn(String name, String color, Point position){
        super(name, color);
        waystoMove = new Point[3];
        piece_position = position;
        waystoMove[0] = new Point(0, 1);
        waystoMove[1] = new Point(-1, 1);
        waystoMove[2] = new Point(1, 1);
        generateAllPossibleMoves();
    }

    @Override
    public void generateAllPossibleMoves() {
        if (this.piece_color.equals("white")){
            //keep the moves the same
        }
        else{
            for (int i = 0; i < waystoMove.length; i++){
                waystoMove[i].y = -1;
            }
        }
    }

}
