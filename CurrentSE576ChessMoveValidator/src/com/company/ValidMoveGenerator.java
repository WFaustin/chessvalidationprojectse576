package com.company;

import java.awt.*;
import java.util.Vector;

public class ValidMoveGenerator {
    //Executive decision. C input is King.

    //private com.company.Input input;
    private Board board;
    private Player [] players;
    private String whiteInput;
    private String blackInput;
    private String movingPiece;
    private Piece selectedPiece;
    private Input scanner = Input.getInstance();
    private Ruleset ruleset;
    Converter converter;


    public ValidMoveGenerator(){
        board = new Board();
        players = new Player[2];
        players[0] = new Player("white");
        players[1] = new Player("black");
        ruleset = new Ruleset();
        converter = new Converter();
        StartSimulation();
    }

    private void StartSimulation(){
        scanner.getOutput().printOutput("This Is a Chess Move Validation Simulation. Please do the following:");
        scanner.getOutput().printOutput("Enter all pieces on the board for Player #1. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).");

        whiteInput = scanner.getScanner().nextLine();

        scanner.getOutput().printOutput("Enter all pieces on the board for Player #2. Put the Initial of the Piece, then the row (A - H), then the column (1 - 8).");

        blackInput = scanner.getScanner().nextLine();

        scanner.getOutput().printOutput("Enter the piece that should be checked for moves.");

        movingPiece = scanner.getScanner().nextLine();

        scanner.getOutput().printOutput("Printing out valid moves.");

        parseandPlay(movingPiece);
    }

    private void parseandPlay(String movingPiece){
        String [] whitefinal = whiteInput.split(" ");


        for (int i = 0; i < whitefinal.length; i++){
            whitefinal[i] =  converter.setStringForInput(whitefinal[i]);
            createPieceAndPosition(whitefinal[i], players[0]);
            board.putPieceOnBoard(players[0].getPiece(i), players[0].getPiece(i).getPiece_position().x, players[0].getPiece(i).getPiece_position().y);
        }

        String [] blackfinal = blackInput.split(" ");

        for (int i = 0; i < blackfinal.length; i++){
            blackfinal[i] = converter.setStringForInput(blackfinal[i]);
            createPieceAndPosition(blackfinal[i], players[1]);
            board.putPieceOnBoard(players[1].getPiece(i), players[1].getPiece(i).getPiece_position().x, players[1].getPiece(i).getPiece_position().y);
        }

        //check if movingPiece is in either of the split arrays. If not, no valid moves. If so, we check the moves of that piece at that position.
        boolean movingPieceFound = false;
        movingPiece = converter.setStringForInput(movingPiece);

        for (int i = 0; i < players.length; i++) {
            if (i == 0) {
                for (int j = 0; j < whitefinal.length; j++) {
                    if (movingPiece.equals(whitefinal[j])) {
                        movingPieceFound = true;
                        break;
                    }
                    if (movingPieceFound == true) {
                        break;
                    }
                }
            } else if (i == 1) {
                for (int j = 0; j < blackfinal.length; j++) {
                    if (movingPiece.equals(blackfinal[j])) {
                        movingPieceFound = true;
                        break;
                    }
                    if (movingPieceFound == true) {
                        break;
                    }
                }
            }
        }

        if (movingPieceFound == false){
            scanner.getOutput().printOutput("The Piece That You Searched For Does Not Appear To Be On The Board. Please Try Again.");
        }
        else{
            int s1 = Integer.parseInt(Character.toString(movingPiece.charAt(1)));
            int s2 = Integer.parseInt(Character.toString(movingPiece.charAt(2)));
            selectedPiece = board.getPiece(s1, s2);
            Point[] validMoves = ruleset.getValidMoveForPiece(board.getPiece(s1, s2), board);
            scanner.getOutput().printOutput("The " + selectedPiece.getPiece_color() +" " + selectedPiece.piece_name + " at square " + selectedPiece.getPiece_position().x + ", " + selectedPiece.getPiece_position().y  + " can move in the following places.");
            if (validMoves.length == 0){
                scanner.getOutput().printOutput("There could not be ay valid moves found on the board.");
            }
            for (int l = 0; l < validMoves.length; l++){
                printMove(validMoves[l]);
            }
        }
    }


    private void createPieceAndPosition(String piece, Player player){
        String piece0 = Character.toString(piece.charAt(0));
        String piece1 = Character.toString(piece.charAt(1));
        String piece2 = Character.toString(piece.charAt(2));
        if (checkValidityOfInput(piece0, 0) && checkValidityOfInput(piece1, 1) && checkValidityOfInput(piece2, 2)){
            createPieceForPlayer(player, piece0, piece1, piece2);
        }
    }


    private boolean checkValidityOfInput(String s, int i){
        if (i == 0 && (s.equals("P") || s.equals("p") || s.equals("R") || s.equals("r") || s.equals("K") || s.equals("k") || s.equals("B") || s.equals("b") || s.equals("Q") || s.equals("q") || s.equals("C") || s.equals("c"))){
            return true;
        }
        else if ((i == 1 || i == 2)&& (Integer.parseInt(s) >= 0 && Integer.parseInt(s) < 8)){
            return true;
        }
        else{
            return false;
        }
    }

    private void createPieceForPlayer(Player player, String s0, String s1, String s2){
        player.createPiece(s0, Integer.parseInt(s1), Integer.parseInt(s2));
    }

    private void printMove(Point move){
        scanner.getOutput().printOutput(converter.setStringForOutput(Integer.toString(move.x + selectedPiece.getPiece_position().x)+ Integer.toString(move.y + selectedPiece.getPiece_position().y)));
    }


}
