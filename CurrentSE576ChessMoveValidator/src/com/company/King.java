package com.company;

import java.awt.*;

public class King extends Piece{

    public King(String name, String color, Point position){
        super(name, color);
        waystoMove = new Point[8];
        piece_position = position;
        waystoMove[0] = new Point(1, 1);
        waystoMove[1] = new Point(1, -1);
        waystoMove[2] = new Point(-1, -1);
        waystoMove[3] = new Point(-1, 1);
        waystoMove[4] = new Point(0, 1);
        waystoMove[5] = new Point(0, -1);
        waystoMove[6] = new Point(1, 0);
        waystoMove[7] = new Point(-1, 0);
    }
}
